const filterReducers = (state = { selectedId: 'all' }, action) => {
  switch (action.type) {
    case 'CHANGE_FILTER':
      return { ...state, selectedId: action.payload }
    default:
      return state
  }
}

export default filterReducers