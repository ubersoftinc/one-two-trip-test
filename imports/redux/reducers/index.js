import { combineReducers } from 'redux'

import filterReducers from './filter'

export default combineReducers({ filter: filterReducers })