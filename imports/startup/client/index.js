import { Meteor } from 'meteor/meteor'
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'

import { store } from '../../redux/store'

import App from '../../ui/pages/App'

Meteor.startup(() => {  
  render(
    <Provider store={ store }>
      <App />
    </Provider>,
    document.getElementById('app')
  )
})