import React, { Component } from 'react'
import moment from 'moment'

import '../styles/card.less'

class Card extends Component {
  render() {
    const { flight, color } = this.props

    return (
      <div className="card__cont">
        <div className="card__company" style={ {
          backgroundColor: color,
        } }>
          { flight.carrier }
        </div>
        <div className="card__info">
          <div className="card__departure">
            <div>{ flight.direction.from }</div>
            <div className="card__date">{ moment(flight.departure).format('LLL') }</div>
          </div>
          <div>--></div>
          <div className="card__arrival">
            <div>{ flight.direction.to }</div>
            <div className="card__date">{ moment(flight.arrival).format('LLL') }</div>
          </div>
        </div>
      </div>
    )
  }
}

export default Card
