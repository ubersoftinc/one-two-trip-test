import React, { Component } from 'react'
import { connect } from 'react-redux'
import { _ } from 'lodash'

import data from '../../api/data.json'

import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css'

import '../styles/main.less'

import { changeFilter } from '../../redux/actions'

import Card from '../elements/Card'

function mapStateToProps(state) {  
  return { ...state }
}

function mapDispatchToProps(dispatch) {  
  return {
    changeFilter: selectedCompany => dispatch(changeFilter(selectedCompany)),
  }
}

class App extends Component {
  constructor() {
    super()
    
    const { flights } = data
    
    const carriers = {
      all: {
        title: 'Все авиакомпании',
      },
    }
    
    const colors = {
      'S7': '#C3D600',
      'Aeroflot': '#2C6CAA',
      'KLM': '#00A0E7',
    }

    for (const flight of flights) {
      if (!_.has(carriers, flight.carrier)) {
        carriers[flight.carrier] = {
          title: flight.carrier
        }
      }
    }

    this.state = {
      carriers,
      colors,
    }
  }

  filterChange(id) {
    const { changeFilter } = this.props
    const { selectedId } = this.props.filter

    if (selectedId.id === id) return
    
    changeFilter(id)
  }

  render() {
    const { flights } = data
    const { carriers, colors } = this.state
    const { selectedId } = this.props.filter
    const carriersKeys = _.keys(carriers)
    
    let filteredFlights
    if (selectedId !== 'all') {
      filteredFlights = _.filter(flights, ['carrier', selectedId])
    } else {
      filteredFlights = flights
    }

    return (
      <div className="container">
        <div className="row" style={ {
          marginTop: '3rem',
        } }>
          <div className="col">
            <div className="btn-group">
              <button type="button" className="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                { carriers[selectedId].title }
              </button>
              <div className="dropdown-menu">
                { carriersKeys.map(carrierId => (
                  <a className={ `dropdown-item${ selectedId === carrierId ? ' active' : '' }` } href="javascript:" key={ carrierId } onClick={ () => this.filterChange(carrierId) }>{ carriers[carrierId].title }</a>
                )) }
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <div className="flights__grid">
              { filteredFlights.map(flight => (
                <Card flight={ flight } color={ colors[flight.carrier] } key={ flight.id }/>
              )) }
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)